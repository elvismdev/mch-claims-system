<?php

// prevent direct access
$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
if(!$isAjax) {
    $user_error = 'Access denied - not an AJAX request...';
    trigger_error($user_error, E_USER_ERROR);
}

require('client.inc.php');

// get what user typed in autocomplete input
$term = trim($_GET['term']);

$query = 'SELECT id, address FROM ost_clients_address WHERE address LIKE '. db_input("%$term%");

$res = db_query($query);
$json = array();

if (db_affected_rows()) {
    while($result=db_fetch_array($res)) {
        $row['id'] = $result['id'];
        $row['value'] = $result['address'];
        $row['label'] = $result['address'];
        array_push($json, $row);
    }
}

print json_encode($json);

?>